import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { stringify } from 'querystring';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'json-module',
  templateUrl: 'app/components/auth/json.component.html'
})

export class JsonComponent {
  data: any;
  dataEdit: any;
  dataAdd:any;
  dataProduct:any;
  isLoadData: boolean = false;
  isformEdit: boolean = false;
  isformAdd: boolean = false;
  id: number;
  constructor(private http: Http) {
        this.http.get('http://demo.swap-ez.com:3002/users').map((res: Response) => res.json()).subscribe(
          data => {
            this.data = data;
          },
          err => console.error(err)
        );
    }
  showEdit(id: string) {
    this.id = parseInt(id);
    this.id--;
    this.isformEdit = true;
  }
  showAdd(){
    this.isformAdd=true;
  }
  hideEditForm() {
    this.isformEdit = false;
    this.isformAdd=false;
  }
  hideData() {
    this.isLoadData = false;
  }
  loadData() {
    this.http.get('http://demo.swap-ez.com:3002/users').map((res: Response) => res.json()).subscribe(
      data => {
        this.data = data;
        this.isLoadData = true;
      },
      err => console.error(err)
    );

  }
  setData(){
    this.http.get('http://demo.swap-ez.com:3002/users').map((res: Response) => res.json()).subscribe(
      data => {
        this.data = data;
      },
      err => console.error(err)
    );
  }
  getData(): any {
    return this.data;
  }

  editData(account: string, password: string, firstName: string, lastName: string) {
    var ii = this.id + 1;
    console.log('http://demo.swap-ez.com:3002/users/' + ii);
    this.http.get('http://demo.swap-ez.com:3002/users/' + ii).map((res: Response) => res.json()).subscribe(
      data => {
        this.dataEdit = data;
        this.afterEdit(account, password, firstName, lastName);
      },
      err => console.error(err)
    );

  }
  afterEdit(account: string, password: string, firstName: string, lastName: string) {
    console.log(this.dataEdit.id);
    this.dataEdit.security_account.account = account;
    this.dataEdit.security_account.password = password;
    this.dataEdit.personal_info.first_name = firstName;
    this.dataEdit.personal_info.last_name = lastName;
    console.log(JSON.stringify(this.dataEdit));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, method: "put" });
    let bodyPost = JSON.stringify(this.dataEdit);
    var ii = this.id + 1;
    this.http.put('http://demo.swap-ez.com:3002/users/' + ii, bodyPost, options).map((res: Response) => res.json()).subscribe(
      data => {
      },
      err => console.error(err)
    );
    console.log('Finish !');
    this.isformEdit = false;
  }
  deleteData(id: string) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, method: "delete" });
    let bodyPost = JSON.stringify(this.dataEdit);
    this.http.delete('http://demo.swap-ez.com:3002/users/' + id, options).map((res: Response) => res.json()).subscribe(
      data => {
      },
      err => console.error(err)
    );
  }
  addData(account: string, password: string, firstName: string, lastName: string) {
    console.log(account + " , " + password + " , " + firstName + " , " + lastName);
    console.log('http://demo.swap-ez.com:3002/users/1');
    this.http.get('http://demo.swap-ez.com:3002/users/1').map((res: Response) => res.json()).subscribe(
      data => {
        this.dataAdd = data;
        this.afterAdd(account, password, firstName, lastName);
      },
      err => console.error(err)
    );
  }


//HAN//
  addDataRegister(account: string, password: string, firtsname2:string, lastname2:string, phone:number,adress:string,codeuser:number,question:string,answer:string) {
    console.log(account + " , " + password," , " +firtsname2,  " , ",+lastname2, " , ",+phone, " , ",+adress,",",+codeuser);
    console.log('http://demo.swap-ez.com:3002/users/1');
    this.http.get('http://demo.swap-ez.com:3002/users/1').map((res: Response) => res.json()).subscribe(
      data => {
        this.dataAdd = data;
        this.afterAddRegister(account,password,firtsname2,lastname2,phone,adress,codeuser,question,answer);
      },
      err => console.error(err)
    );
  }
  afterAddRegister(account: string, password: string, firtsname2 : string, lastname2: string, phone: number, adress: string,codeuser:number,question:string,answer:string){
    var count=this.data.length;
    count++;
    this.dataAdd.id=count;
    this.dataAdd.security_account.account = account;
    this.dataAdd.security_account.password = password;
    this.dataAdd.personal_info.first_name=firtsname2;
    this.dataAdd.personal_info.last_name=lastname2;
    this.dataAdd.personal_info.number_phone=phone;
    this.dataAdd.personal_info.address=adress;
    this.dataAdd.personal_info.codeuser=codeuser;
    this.dataAdd.personal_info.question=question;
    this.dataAdd.personal_info.answer=answer;

    console.log(JSON.stringify(this.dataAdd));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, method: "post" });
    let bodyPost = JSON.stringify(this.dataAdd);
    this.http.put('http://demo.swap-ez.com:3002/users/', bodyPost, options).map((res: Response) => res.json()).subscribe(
      data => {
      },
      err => console.error(err)
    );
    this.isformAdd = false;
  }
  //END
  afterAdd(account: string, password: string, firstName: string, lastName: string){
    var count=this.data.length;
    count++;
    this.dataAdd.id=count;
    this.dataAdd.security_account.account = account;
    this.dataAdd.security_account.password = password;
    this.dataAdd.personal_info.first_name = firstName;
    this.dataAdd.personal_info.last_name = lastName;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, method: "post" });
    let bodyPost = JSON.stringify(this.dataAdd);
    this.http.put('http://demo.swap-ez.com:3002/users/', bodyPost, options).map((res: Response) => res.json()).subscribe(
      data => {
        location.reload();
      },
      err => console.error(err)
    );
    this.isformAdd = false;
  }

  addDataProduct(imei: string, tile: string, price:number, shortDescription:string,fullDescription:string,status:any,Category:any)
  {
    this.http.get('http://demo.swap-ez.com:3002/product_detail/1').map((res: Response) => res.json()).subscribe(
      data => {
        this.dataProduct = data;
        this.afterAddDataProduct(imei,tile,price,shortDescription,fullDescription,Category,status);
      },
      err => console.error(err)
    );

  }
  afterAddDataProduct(imei: string, tile: string, price:number, shortDescription:string,fullDescription:string,status:any,Category:any)
  {
    var count=this.dataProduct.length;
    count++;
    this.dataProduct.id=count;
    this.dataProduct.IMEI = imei;
    this.dataProduct.status = status;
    this.dataProduct.title = tile;
    this.dataProduct.price= price;
    this.dataProduct.shortDescription = shortDescription;
    this.dataProduct.description = fullDescription;
    this.dataProduct.Category = Category;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, method: "post" });
    let bodyPost = JSON.stringify(this.dataProduct);
    this.http.put('http://demo.swap-ez.com:3002/product_detail/', bodyPost, options).map((res: Response) => res.json()).subscribe(
      data => {
        location.reload();
      },
      err => console.error(err)
    );
    this.isformAdd = false;

  }

}
