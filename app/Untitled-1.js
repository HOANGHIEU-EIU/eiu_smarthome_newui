"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var JsonComponent = (function () {
    function JsonComponent(http) {
        var _this = this;
        this.http = http;
        this.isLoadData = false;
        this.isformEdit = false;
        this.isformAdd = false;
        this.http.get('http://demo.swap-ez.com:3002/users').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
        }, function (err) { return console.error(err); });
    }
    JsonComponent.prototype.showEdit = function (id) {
        this.id = parseInt(id);
        this.id--;
        this.isformEdit = true;
    };
    JsonComponent.prototype.showAdd = function () {
        this.isformAdd = true;
    };
    JsonComponent.prototype.hideEditForm = function () {
        this.isformEdit = false;
        this.isformAdd = false;
    };
    JsonComponent.prototype.hideData = function () {
        this.isLoadData = false;
    };
    JsonComponent.prototype.loadData = function () {
        var _this = this;
        this.http.get('http://demo.swap-ez.com:3002/users').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
            _this.isLoadData = true;
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.setData = function () {
        var _this = this;
        this.http.get('http://demo.swap-ez.com:3002/users').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.data = data;
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.getData = function () {
        return this.data;
    };
    JsonComponent.prototype.editData = function (account, password, firstName, lastName) {
        var _this = this;
        var ii = this.id + 1;
        console.log('http://demo.swap-ez.com:3002/users/' + ii);
        this.http.get('http://demo.swap-ez.com:3002/users/' + ii).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.dataEdit = data;
            _this.afterEdit(account, password, firstName, lastName);
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.afterEdit = function (account, password, firstName, lastName) {
        console.log(this.dataEdit.id);
        this.dataEdit.security_account.account = account;
        this.dataEdit.security_account.password = password;
        this.dataEdit.personal_info.first_name = firstName;
        this.dataEdit.personal_info.last_name = lastName;
        console.log(JSON.stringify(this.dataEdit));
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: "put" });
        var bodyPost = JSON.stringify(this.dataEdit);
        var ii = this.id + 1;
        this.http.put('http://demo.swap-ez.com:3002/users/' + ii, bodyPost, options).map(function (res) { return res.json(); }).subscribe(function (data) {
        }, function (err) { return console.error(err); });
        console.log('Finish !');
        this.isformEdit = false;
    };
    JsonComponent.prototype.deleteData = function (id) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: "delete" });
        var bodyPost = JSON.stringify(this.dataEdit);
        this.http.delete('http://demo.swap-ez.com:3002/users/' + id, options).map(function (res) { return res.json(); }).subscribe(function (data) {
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.addData = function (account, password, firstName, lastName) {
        var _this = this;
        console.log(account + " , " + password + " , " + firstName + " , " + lastName);
        console.log('http://demo.swap-ez.com:3002/users/1');
        this.http.get('http://demo.swap-ez.com:3002/users/1').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.dataAdd = data;
            _this.afterAdd(account, password, firstName, lastName);
        }, function (err) { return console.error(err); });
    };
    //HAN//
    JsonComponent.prototype.addDataRegister = function (account, password, firtsname2, lastname2, phone, adress, codeuser, question, answer) {
        var _this = this;
        console.log(account + " , " + password, " , " + firtsname2, " , ", +lastname2, " , ", +phone, " , ", +adress, ",", +codeuser);
        console.log('http://demo.swap-ez.com:3002/users/1');
        this.http.get('http://demo.swap-ez.com:3002/users/1').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.dataAdd = data;
            _this.afterAddRegister(account, password, firtsname2, lastname2, phone, adress, codeuser, question, answer);
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.afterAddRegister = function (account, password, firtsname2, lastname2, phone, adress, codeuser, question, answer) {
        var count = this.data.length;
        count++;
        this.dataAdd.id = count;
        this.dataAdd.security_account.account = account;
        this.dataAdd.security_account.password = password;
        this.dataAdd.personal_info.first_name = firtsname2;
        this.dataAdd.personal_info.last_name = lastname2;
        this.dataAdd.personal_info.number_phone = phone;
        this.dataAdd.personal_info.address = adress;
        this.dataAdd.personal_info.codeuser = codeuser;
        this.dataAdd.personal_info.question = question;
        this.dataAdd.personal_info.answer = answer;
        console.log(JSON.stringify(this.dataAdd));
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: "post" });
        var bodyPost = JSON.stringify(this.dataAdd);
        this.http.put('http://demo.swap-ez.com:3002/users/', bodyPost, options).map(function (res) { return res.json(); }).subscribe(function (data) {
        }, function (err) { return console.error(err); });
        this.isformAdd = false;
    };
    //END
    JsonComponent.prototype.afterAdd = function (account, password, firstName, lastName) {
        var count = this.data.length;
        count++;
        this.dataAdd.id = count;
        this.dataAdd.security_account.account = account;
        this.dataAdd.security_account.password = password;
        this.dataAdd.personal_info.first_name = firstName;
        this.dataAdd.personal_info.last_name = lastName;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: "post" });
        var bodyPost = JSON.stringify(this.dataAdd);
        this.http.put('http://demo.swap-ez.com:3002/users/', bodyPost, options).map(function (res) { return res.json(); }).subscribe(function (data) {
            location.reload();
        }, function (err) { return console.error(err); });
        this.isformAdd = false;
    };
    JsonComponent.prototype.addDataProduct = function (imei, tile, price, shortDescription, fullDescription, status, Category) {
        var _this = this;
        this.http.get('http://demo.swap-ez.com:3002/product_detail/1').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.dataProduct = data;
            _this.afterAddDataProduct(imei, tile, price, shortDescription, fullDescription, Category, status);
        }, function (err) { return console.error(err); });
    };
    JsonComponent.prototype.afterAddDataProduct = function (imei, tile, price, shortDescription, fullDescription, status, Category) {
        var count = this.dataProduct.length;
        count++;
        this.dataProduct.id = count;
        this.dataProduct.IMEI = imei;
        this.dataProduct.status = status;
        this.dataProduct.title = tile;
        this.dataProduct.price = price;
        this.dataProduct.shortDescription = shortDescription;
        this.dataProduct.description = fullDescription;
        this.dataProduct.Category = Category;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers, method: "post" });
        var bodyPost = JSON.stringify(this.dataProduct);
        this.http.put('http://demo.swap-ez.com:3002/product_detail/', bodyPost, options).map(function (res) { return res.json(); }).subscribe(function (data) {
            location.reload();
        }, function (err) { return console.error(err); });
        this.isformAdd = false;
    };
    JsonComponent = __decorate([
        core_1.Component({
            selector: 'json-module',
            templateUrl: 'app/components/auth/json.component.html'
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], JsonComponent);
    return JsonComponent;
}());
exports.JsonComponent = JsonComponent;
//# sourceMappingURL=Untitled-1.js.map