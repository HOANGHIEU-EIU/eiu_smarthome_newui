import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Route} from '@angular/router';
import {HttpModule, Http} from '@angular/http';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {SidebarModule} from './sidebar/sidebar.module';
import {FooterModule} from './shared/footer/footer.module';
import {NavbarModule} from './shared/navbar/navbar.module';
import {HashLocationStrategy, LocationStrategy, CommonModule} from '@angular/common';
import {ConnectService} from "./services/connect.service";
import {LightService} from './services/light.service';
import {TemperatureService} from './services/temperature.service';
import {FanService} from './services/fan.service';


export const MODULE_ROUTES: Route[] = [
]

@NgModule({
    imports: [
        BrowserModule,
        DashboardModule,
        SidebarModule,
        CommonModule,
        NavbarModule,
        FooterModule,
        RouterModule.forRoot([]),
        HttpModule
    ],

    declarations: [AppComponent, DashboardComponent],
    providers: [
        ConnectService,
        LightService,
        TemperatureService,
        FanService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
