import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MODULE_COMPONENTS, MODULE_ROUTES} from './dashboard.routes';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
    imports: [
        RouterModule.forChild(MODULE_ROUTES),
        CommonModule,
        BrowserModule
    ],
    declarations: [MODULE_COMPONENTS],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DashboardModule {
}
