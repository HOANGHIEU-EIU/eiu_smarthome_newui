import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { IconsComponent } from './icons/icons.component';
import { TableComponent } from './table/table.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TypographyComponent } from './typography/typography.component';
import { MapsComponent } from './maps/maps.component';
import { LivingroomComponent } from './home/livingroom/livingroom.component';
import { ChildrenComponent } from './home/childrenroom/childrenroom.component';
import { ParentComponent } from './home/parentroom/parentroom.component';
import { KitchenComponent } from './home/kitchen/kitchen.compoment';
import { GardenComponent } from './home/garden/garden.compoment';
import { GarageComponent } from './home/garage/garage.compoment';
import { LoginComponent } from '../login/login.component';
import {AboutUs} from './about/about.component';

export const MODULE_ROUTES: Route[] = [
  { path: 'dashboard', component: HomeComponent },
  { path: 'user', component: UserComponent },
  { path: 'livingroom', component: LivingroomComponent },
  { path: 'table', component: TableComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'notifications', component: NotificationsComponent },
  { path: 'typography', component: TypographyComponent },
  { path: 'aboutus', component: AboutUs },
  //{ path: 'maps', component: MapsComponent },
 //{ path: '', redirectTo: 'dashboard', pathMatch: 'full' }
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent }
]

export const MODULE_COMPONENTS = [
  HomeComponent,
  UserComponent,
  TableComponent,
  IconsComponent,
  NotificationsComponent,
  LivingroomComponent,
  ParentComponent,
  ChildrenComponent,
  KitchenComponent,
  GardenComponent,
  GarageComponent,
  TypographyComponent,
  MapsComponent,
  LoginComponent,
  AboutUs
]
