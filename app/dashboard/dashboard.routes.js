"use strict";
var home_component_1 = require('./home/home.component');
var user_component_1 = require('./user/user.component');
var icons_component_1 = require('./icons/icons.component');
var table_component_1 = require('./table/table.component');
var notifications_component_1 = require('./notifications/notifications.component');
var typography_component_1 = require('./typography/typography.component');
var maps_component_1 = require('./maps/maps.component');
var livingroom_component_1 = require('./home/livingroom/livingroom.component');
var childrenroom_component_1 = require('./home/childrenroom/childrenroom.component');
var parentroom_component_1 = require('./home/parentroom/parentroom.component');
var kitchen_compoment_1 = require('./home/kitchen/kitchen.compoment');
var garden_compoment_1 = require('./home/garden/garden.compoment');
var garage_compoment_1 = require('./home/garage/garage.compoment');
var login_component_1 = require('../login/login.component');
var about_component_1 = require('./about/about.component');
exports.MODULE_ROUTES = [
    { path: 'dashboard', component: home_component_1.HomeComponent },
    { path: 'user', component: user_component_1.UserComponent },
    { path: 'livingroom', component: livingroom_component_1.LivingroomComponent },
    { path: 'table', component: table_component_1.TableComponent },
    { path: 'icons', component: icons_component_1.IconsComponent },
    { path: 'notifications', component: notifications_component_1.NotificationsComponent },
    { path: 'typography', component: typography_component_1.TypographyComponent },
    { path: 'aboutus', component: about_component_1.AboutUs },
    //{ path: 'maps', component: MapsComponent },
    //{ path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent }
];
exports.MODULE_COMPONENTS = [
    home_component_1.HomeComponent,
    user_component_1.UserComponent,
    table_component_1.TableComponent,
    icons_component_1.IconsComponent,
    notifications_component_1.NotificationsComponent,
    livingroom_component_1.LivingroomComponent,
    parentroom_component_1.ParentComponent,
    childrenroom_component_1.ChildrenComponent,
    kitchen_compoment_1.KitchenComponent,
    garden_compoment_1.GardenComponent,
    garage_compoment_1.GarageComponent,
    typography_component_1.TypographyComponent,
    maps_component_1.MapsComponent,
    login_component_1.LoginComponent,
    about_component_1.AboutUs
];
//# sourceMappingURL=dashboard.routes.js.map