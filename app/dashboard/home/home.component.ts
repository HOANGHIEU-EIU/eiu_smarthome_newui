import { Component, OnInit, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { LightService } from '../../services/light.service';
import { TemperatureService } from '../../services/temperature.service';
import { ConnectService } from '../../services/connect.service';
import { LivingroomComponent } from './livingroom/livingroom.component';
import { ParentComponent } from './parentroom/parentroom.component';
import { ChildrenComponent } from './childrenroom/childrenroom.component';
import { KitchenComponent } from './kitchen/kitchen.compoment';
import { GardenComponent } from './garden/garden.compoment';
import { GarageComponent } from './garage/garage.compoment';
import { Router } from '@angular/router';
import initDemo = require('../../../assets/js/charts.js');

declare var $: any;

@Component({
    selector: 'home-cmp',
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {


    livingRoom_Lamp: boolean = false;
    parentRoom_Lamp: boolean = false;
    childrenRoom_Lamp: boolean = false;
    kitchen_Lamp: boolean = false;
    garage_Lamp: boolean = false;
    garden_Lamp: boolean = false;
    @ViewChild(LivingroomComponent) private _LivingroomComponent: LivingroomComponent;
    @ViewChild(ParentComponent) private _ParentComponent: ParentComponent;
    @ViewChild(ChildrenComponent) private _ChildrenComponent: ChildrenComponent;
    @ViewChild(KitchenComponent) private _KitchenComponent: KitchenComponent;
    @ViewChild(GardenComponent) private _GardenComponent: GardenComponent;
    @ViewChild(GarageComponent) private _GarageComponent: GarageComponent;

    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService,
        private _route: Router) {
        this._ConnectService.Connect();
        
    }

    ngOnInit() {
        initDemo();
        if (!sessionStorage.getItem('User'))
            this._route.navigate(['/login']);
    }

    public TurnOnAll() {
        this._LivingroomComponent.LampControll(1);
        this.livingRoom_Lamp = true;

        this._ParentComponent.LampControll(1);
        this.parentRoom_Lamp = true;

        this._ChildrenComponent.LampControll(1);
        this.childrenRoom_Lamp = true;

        this._KitchenComponent.LampControll(1);
        this.kitchen_Lamp = true;

        this._GarageComponent.LampControll(1);
        this.garage_Lamp = true;

        this._GardenComponent.LampControll(1);
        this.garden_Lamp = true;
    }
    public TurnOffAll() {
        this._LivingroomComponent.LampControll(0);
        this.livingRoom_Lamp = false;

        this._ParentComponent.LampControll(0);
        this.parentRoom_Lamp = false;

        this._ChildrenComponent.LampControll(0);
        this.childrenRoom_Lamp = false;


        this._KitchenComponent.LampControll(0);
        this.kitchen_Lamp = false;

        this._GarageComponent.LampControll(0);
        this.garage_Lamp = false;

        this._GardenComponent.LampControll(0);
        this.garden_Lamp = false;
    }

}
