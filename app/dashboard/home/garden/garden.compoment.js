"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var light_service_1 = require('../../../services/light.service');
var connect_service_1 = require('../../../services/connect.service');
var temperature_service_1 = require('../../../services/temperature.service');
var GardenComponent = (function () {
    function GardenComponent(_LightService, _ConnectService, _TemperatureService) {
        this._LightService = _LightService;
        this._ConnectService = _ConnectService;
        this._TemperatureService = _TemperatureService;
        this.socket = this._ConnectService.SocketInstanse();
        this.isChecked = false;
    }
    GardenComponent.prototype.ngOnChanges = function (changes) {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    };
    GardenComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(function (data) {
            _this.temperature = JSON.stringify(data['temperature']);
            _this.fahrenheit = JSON.stringify(data['fahrenheit']);
        });
    };
    GardenComponent.prototype.LampControll = function (param) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }
        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'garden_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'garden_lamp');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], GardenComponent.prototype, "statusControll", void 0);
    GardenComponent = __decorate([
        core_1.Component({
            selector: 'garden',
            moduleId: module.id,
            templateUrl: 'garden.compoment.html'
        }), 
        __metadata('design:paramtypes', [light_service_1.LightService, connect_service_1.ConnectService, temperature_service_1.TemperatureService])
    ], GardenComponent);
    return GardenComponent;
}());
exports.GardenComponent = GardenComponent;
//# sourceMappingURL=garden.compoment.js.map