import { Component, OnInit, SimpleChanges, OnChanges, Input } from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';

@Component({
    selector: 'garden',
    moduleId: module.id,
    templateUrl: 'garden.compoment.html'
})

export class GardenComponent implements OnInit, OnChanges {
    dataTemperature: any;
    value: any;
    temperature: string;
    fahrenheit: string;
    isChecked: boolean;
    socket: any = this._ConnectService.SocketInstanse();
    @Input() statusControll: boolean;


    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService) {
        this.isChecked = false;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }


    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
                this.fahrenheit = JSON.stringify(data['fahrenheit']);
            })
    }

    public LampControll(param: any) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }

        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'garden_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'garden_lamp');
    }
}