"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var light_service_1 = require('../../../services/light.service');
var connect_service_1 = require('../../../services/connect.service');
var temperature_service_1 = require('../../../services/temperature.service');
var fan_service_1 = require('../../../services/fan.service');
var ParentComponent = (function () {
    function ParentComponent(_LightService, _ConnectService, _TemperatureService, _FanService) {
        this._LightService = _LightService;
        this._ConnectService = _ConnectService;
        this._TemperatureService = _TemperatureService;
        this._FanService = _FanService;
        this.value = false;
        this.statusFan = false;
        this.socket = this._ConnectService.SocketInstanse();
        this.isChecked = false;
    }
    ParentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(function (data) {
            _this.temperature = JSON.stringify(data['temperature']);
            _this.fahrenheit = JSON.stringify(data['fahrenheit']);
        });
    };
    ParentComponent.prototype.ngOnChanges = function (changes) {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    };
    ParentComponent.prototype.LampControll = function (param) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }
        if (this.value) {
            this._LightService.TurnOnLight(this.socket, 'parentroom_lamp');
        }
        else {
            this._LightService.TurnOffLight(this.socket, 'parentroom_lamp');
        }
    };
    ParentComponent.prototype.FanControll = function () {
        this.statusFan = !this.statusFan;
        if (this.statusFan)
            this._FanService.TurnOnFan(this.socket, 'FanOn');
        else
            this._FanService.TurnOffFan(this.socket, 'FanOff');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ParentComponent.prototype, "statusControll", void 0);
    ParentComponent = __decorate([
        core_1.Component({
            selector: 'parent',
            moduleId: module.id,
            templateUrl: 'parentroom.component.html',
            providers: [fan_service_1.FanService]
        }), 
        __metadata('design:paramtypes', [light_service_1.LightService, connect_service_1.ConnectService, temperature_service_1.TemperatureService, fan_service_1.FanService])
    ], ParentComponent);
    return ParentComponent;
}());
exports.ParentComponent = ParentComponent;
//# sourceMappingURL=parentroom.component.js.map