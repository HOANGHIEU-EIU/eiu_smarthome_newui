import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';
import { FanService } from '../../../services/fan.service';
@Component({
    selector: 'parent',
    moduleId: module.id,
    templateUrl: 'parentroom.component.html',
    providers: [FanService]
})

export class ParentComponent implements OnInit {

    dataTemperature: any;
    dataLight: any;
    temperature: string;
    fahrenheit: string;
    value: boolean = false;
    statusFan: boolean = false;
    socket: any = this._ConnectService.SocketInstanse();
    @Input() statusControll: boolean;
    isChecked: boolean;


    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService,
        private _FanService: FanService) {
        this.isChecked = false;
    }

    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
                this.fahrenheit = JSON.stringify(data['fahrenheit']);
            })
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }


    public LampControll(param: any) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }

        if (this.value) {
            this._LightService.TurnOnLight(this.socket, 'parentroom_lamp');
        }
        else {
            this._LightService.TurnOffLight(this.socket, 'parentroom_lamp');
        }
    }

    public FanControll() {
        this.statusFan = !this.statusFan;
        if (this.statusFan)
            this._FanService.TurnOnFan(this.socket, 'FanOn');
        else
            this._FanService.TurnOffFan(this.socket, 'FanOff');
    }
}