"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var light_service_1 = require('../../../services/light.service');
var connect_service_1 = require('../../../services/connect.service');
var temperature_service_1 = require('../../../services/temperature.service');
var GarageComponent = (function () {
    function GarageComponent(_LightService, _ConnectService, _TemperatureService) {
        this._LightService = _LightService;
        this._ConnectService = _ConnectService;
        this._TemperatureService = _TemperatureService;
        this.socket = this._ConnectService.SocketInstanse();
        this.valueDoor = false;
        this.value = false;
        this.isChecked = false;
    }
    GarageComponent.prototype.ngOnChanges = function (changes) {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    };
    GarageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(function (data) {
            _this.temperature = JSON.stringify(data['temperature']);
            _this.fahrenheit = JSON.stringify(data['fahrenheit']);
        });
    };
    GarageComponent.prototype.DoorControll = function () {
        this.valueDoor = !this.valueDoor;
        if (this.valueDoor) {
            this.socket.emit('openDoor');
        }
        else {
            this.socket.emit('closeDoor');
        }
    };
    GarageComponent.prototype.LampControll = function (param) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }
        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'garage_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'garage_lamp');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], GarageComponent.prototype, "statusControll", void 0);
    GarageComponent = __decorate([
        core_1.Component({
            selector: 'garage',
            moduleId: module.id,
            templateUrl: 'garage.compoment.html'
        }), 
        __metadata('design:paramtypes', [light_service_1.LightService, connect_service_1.ConnectService, temperature_service_1.TemperatureService])
    ], GarageComponent);
    return GarageComponent;
}());
exports.GarageComponent = GarageComponent;
//# sourceMappingURL=garage.compoment.js.map