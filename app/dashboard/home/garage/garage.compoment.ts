import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';

@Component({
    selector: 'garage',
    moduleId: module.id,
    templateUrl: 'garage.compoment.html'
})

export class GarageComponent implements OnInit {
    dataTemperature: any;
    dataLight: any;
    temperature: string;
    fahrenheit: string;
    socket: any = this._ConnectService.SocketInstanse();
    isChecked: boolean;
    valueDoor: boolean = false;
    value: boolean = false;
    @Input() statusControll: boolean;

    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService) {
        this.isChecked = false;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }

    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
                this.fahrenheit = JSON.stringify(data['fahrenheit']);
            });
    }

    public DoorControll() {
        this.valueDoor = !this.valueDoor;
        if (this.valueDoor) {
            this.socket.emit('openDoor');
        }
        else {
            this.socket.emit('closeDoor');
        }
    }

    public LampControll(param: any) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }

        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'garage_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'garage_lamp');
    }
}