"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var light_service_1 = require('../../services/light.service');
var temperature_service_1 = require('../../services/temperature.service');
var connect_service_1 = require('../../services/connect.service');
var livingroom_component_1 = require('./livingroom/livingroom.component');
var parentroom_component_1 = require('./parentroom/parentroom.component');
var childrenroom_component_1 = require('./childrenroom/childrenroom.component');
var kitchen_compoment_1 = require('./kitchen/kitchen.compoment');
var garden_compoment_1 = require('./garden/garden.compoment');
var garage_compoment_1 = require('./garage/garage.compoment');
var initDemo = require('../../../assets/js/charts.js');
var HomeComponent = (function () {
    function HomeComponent(_LightService, _ConnectService, _TemperatureService) {
        this._LightService = _LightService;
        this._ConnectService = _ConnectService;
        this._TemperatureService = _TemperatureService;
        this.livingRoom_Lamp = false;
        this.parentRoom_Lamp = false;
        this.childrenRoom_Lamp = false;
        this.kitchen_Lamp = false;
        this.garage_Lamp = false;
        this.garden_Lamp = false;
        this._ConnectService.Connect();
    }
    HomeComponent.prototype.ngOnInit = function () {
        initDemo();
    };
    HomeComponent.prototype.TurnOnAll = function () {
        this._LivingroomComponent.LampControll(1);
        this.livingRoom_Lamp = true;
        this._ParentComponent.LampControll(1);
        this.parentRoom_Lamp = true;
        this._ChildrenComponent.LampControll(1);
        this.childrenRoom_Lamp = true;
        this._KitchenComponent.LampControll(1);
        this.kitchen_Lamp = true;
        this._GarageComponent.LampControll(1);
        this.garage_Lamp = true;
        this._GardenComponent.LampControll(1);
        this.garden_Lamp = true;
    };
    HomeComponent.prototype.TurnOffAll = function () {
        this._LivingroomComponent.LampControll(0);
        this.livingRoom_Lamp = false;
        this._ParentComponent.LampControll(0);
        this.parentRoom_Lamp = false;
        this._ChildrenComponent.LampControll(0);
        this.childrenRoom_Lamp = false;
        this._KitchenComponent.LampControll(0);
        this.kitchen_Lamp = false;
        this._GarageComponent.LampControll(0);
        this.garage_Lamp = false;
        this._GardenComponent.LampControll(0);
        this.garden_Lamp = false;
    };
    __decorate([
        core_1.ViewChild(livingroom_component_1.LivingroomComponent), 
        __metadata('design:type', livingroom_component_1.LivingroomComponent)
    ], HomeComponent.prototype, "_LivingroomComponent", void 0);
    __decorate([
        core_1.ViewChild(parentroom_component_1.ParentComponent), 
        __metadata('design:type', parentroom_component_1.ParentComponent)
    ], HomeComponent.prototype, "_ParentComponent", void 0);
    __decorate([
        core_1.ViewChild(childrenroom_component_1.ChildrenComponent), 
        __metadata('design:type', childrenroom_component_1.ChildrenComponent)
    ], HomeComponent.prototype, "_ChildrenComponent", void 0);
    __decorate([
        core_1.ViewChild(kitchen_compoment_1.KitchenComponent), 
        __metadata('design:type', kitchen_compoment_1.KitchenComponent)
    ], HomeComponent.prototype, "_KitchenComponent", void 0);
    __decorate([
        core_1.ViewChild(garden_compoment_1.GardenComponent), 
        __metadata('design:type', garden_compoment_1.GardenComponent)
    ], HomeComponent.prototype, "_GardenComponent", void 0);
    __decorate([
        core_1.ViewChild(garage_compoment_1.GarageComponent), 
        __metadata('design:type', garage_compoment_1.GarageComponent)
    ], HomeComponent.prototype, "_GarageComponent", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home-cmp',
            moduleId: module.id,
            templateUrl: 'home.component.html'
        }), 
        __metadata('design:paramtypes', [light_service_1.LightService, connect_service_1.ConnectService, temperature_service_1.TemperatureService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map