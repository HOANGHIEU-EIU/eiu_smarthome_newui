import { Component, OnInit, trigger, state, style, transition, animate, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';
import initDemo = require('../../../../assets/js/charts.js');

declare var jQuery: any;


@Component({
    selector: 'livingroom',
    moduleId: module.id,
    templateUrl: 'livingroom.component.html'
})

export class LivingroomComponent implements OnInit, OnDestroy, OnChanges {

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }

    interval: any;
    value: boolean = false;
    temperature: any;
    fahrenheit: any;
    dataTemperature: any;
    dataLight: any;
    isChecked: boolean;
    autoMode: boolean = false;
    manualMode: boolean = true;
    socket: any = this._ConnectService.SocketInstanse();
    @Input() statusControll: boolean; // status from cha.


    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService) {
        this.isChecked = false;
    }

    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this.socket)
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
            });

        this.dataLight = this._LightService.AutoLightController(this.socket)
            .subscribe(data => {
                if (JSON.stringify(data['statusLamp']) !== "undefined")
                    this.isChecked = !this.isChecked
            });

    }



    public LampControll(param:any) {
        //chuyen doi qua lai
        if(parseInt(param) === 2){
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if(parseInt(param) === 1){
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if(parseInt(param) === 0){
            this.value = false;
            this.isChecked = false;
        }  

        if (this.value) {
            this.socket.emit('livingroom_lamp', {
                name: "lamp",
                status: "1"
            });
        }
        else {
            this.socket.emit('livingroom_lamp', {
                name: "lamp",
                status: "0"
            });
        }
    }

    

    public SwitchMode() {
        this.manualMode = !this.manualMode;//false
        this.autoMode = !this.autoMode;//true
        if (this.autoMode == true) {
            this.socket.emit('autoMode', {
                status: true
            });
        }
        else {
            this.socket.emit('autoMode', {
                status: false
            });
        }
    }

    antiThiefMode: boolean = false;
    public AntiThiefMode() {
        this.antiThiefMode = !this.antiThiefMode;
        if (this.antiThiefMode == true) {
            this.socket.emit('anti-theif', {
                status: true
            });
        }
        else {
            this.socket.emit('anti-theif', {
                status: false
            });
        }
    }




    TurnOnAllLamps() {
        this.socket.emit('turnOnAllLamps');
    }

    TurnOffAllLamps() {
        this.socket.emit('turnOffAllLamps');
    }

    ngOnDestroy(): void {
        this.dataTemperature.unsubscribe();
    }

}


