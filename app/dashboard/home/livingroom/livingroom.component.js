"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var light_service_1 = require('../../../services/light.service');
var connect_service_1 = require('../../../services/connect.service');
var temperature_service_1 = require('../../../services/temperature.service');
var LivingroomComponent = (function () {
    function LivingroomComponent(_LightService, _ConnectService, _TemperatureService) {
        this._LightService = _LightService;
        this._ConnectService = _ConnectService;
        this._TemperatureService = _TemperatureService;
        this.value = false;
        this.autoMode = false;
        this.manualMode = true;
        this.socket = this._ConnectService.SocketInstanse();
        this.antiThiefMode = false;
        this.isChecked = false;
    }
    LivingroomComponent.prototype.ngOnChanges = function (changes) {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    };
    LivingroomComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataTemperature = this._TemperatureService.GetTemperature(this.socket)
            .subscribe(function (data) {
            _this.temperature = JSON.stringify(data['temperature']);
        });
        this.dataLight = this._LightService.AutoLightController(this.socket)
            .subscribe(function (data) {
            if (JSON.stringify(data['statusLamp']) !== "undefined")
                _this.isChecked = !_this.isChecked;
        });
    };
    LivingroomComponent.prototype.LampControll = function (param) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }
        if (this.value) {
            this.socket.emit('livingroom_lamp', {
                name: "lamp",
                status: "1"
            });
        }
        else {
            this.socket.emit('livingroom_lamp', {
                name: "lamp",
                status: "0"
            });
        }
    };
    LivingroomComponent.prototype.SwitchMode = function () {
        this.manualMode = !this.manualMode; //false
        this.autoMode = !this.autoMode; //true
        if (this.autoMode == true) {
            this.socket.emit('autoMode', {
                status: true
            });
        }
        else {
            this.socket.emit('autoMode', {
                status: false
            });
        }
    };
    LivingroomComponent.prototype.AntiThiefMode = function () {
        this.antiThiefMode = !this.antiThiefMode;
        if (this.antiThiefMode == true) {
            this.socket.emit('anti-theif', {
                status: true
            });
        }
        else {
            this.socket.emit('anti-theif', {
                status: false
            });
        }
    };
    LivingroomComponent.prototype.TurnOnAllLamps = function () {
        this.socket.emit('turnOnAllLamps');
    };
    LivingroomComponent.prototype.TurnOffAllLamps = function () {
        this.socket.emit('turnOffAllLamps');
    };
    LivingroomComponent.prototype.ngOnDestroy = function () {
        this.dataTemperature.unsubscribe();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], LivingroomComponent.prototype, "statusControll", void 0);
    LivingroomComponent = __decorate([
        core_1.Component({
            selector: 'livingroom',
            moduleId: module.id,
            templateUrl: 'livingroom.component.html'
        }), 
        __metadata('design:paramtypes', [light_service_1.LightService, connect_service_1.ConnectService, temperature_service_1.TemperatureService])
    ], LivingroomComponent);
    return LivingroomComponent;
}());
exports.LivingroomComponent = LivingroomComponent;
//# sourceMappingURL=livingroom.component.js.map