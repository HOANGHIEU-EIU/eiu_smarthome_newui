import { Component, OnInit, SimpleChanges, OnChanges, Input } from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';


@Component({
    selector: 'children',
    moduleId: module.id,
    templateUrl: 'childrenroom.component.html'
})

export class ChildrenComponent implements OnInit, OnChanges {
    dataTemperature: any;
    dataLight: any;
    temperature: string;
    fahrenheit: string;
    value: boolean = false;
    socket: any = this._ConnectService.SocketInstanse();
    isChecked: boolean;
    @Input() statusControll: boolean;


    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService) {
        this.isChecked = false;
    }


    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }

    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this.socket)
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
                this.fahrenheit = JSON.stringify(data['fahrenheit']);
            });

        /*this.dataLight = this._LightService.AutoLightController(this.socket)
            .subscribe(data => {
                if (JSON.stringify(data['status']) !== "undefined")
                    this.isChecked = !this.isChecked
            });*/
    }

    public LampControll(param: any) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }

        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'childrenroom_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'childrenroom_lamp');
    }
}