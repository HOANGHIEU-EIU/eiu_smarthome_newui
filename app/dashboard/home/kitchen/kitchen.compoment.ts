import {
    Component, OnInit, trigger,
    state,
    style,
    transition,
    animate,
    keyframes
} from '@angular/core';
import { LightService } from '../../../services/light.service';
import { ConnectService } from '../../../services/connect.service';
import { TemperatureService } from '../../../services/temperature.service';
import { SimpleChanges, OnChanges, Input } from '@angular/core';
declare var jQuery: any;

@Component({
    selector: 'kitchen',
    moduleId: module.id,
    templateUrl: 'kitchen.compoment.html'
})

export class KitchenComponent implements OnInit, OnChanges {
    changeGasStatus: boolean = false;
    dataTemperature: any;
    temperature: string;
    fahrenheit: string;
    value: boolean;
    socket: any = this._ConnectService.SocketInstanse();
    isChecked: boolean;
    @Input() statusControll: boolean;

    constructor(private _LightService: LightService,
        private _ConnectService: ConnectService,
        private _TemperatureService: TemperatureService) {
            this.isChecked = false;
    }


    ngOnChanges(changes: SimpleChanges): void {
        if (changes['statusControll']) {
            this.isChecked = changes['statusControll'].currentValue;
        }
    }

    ngOnInit(): void {
        this.dataTemperature = this._TemperatureService.GetTemperature(this._ConnectService.SocketInstanse())
            .subscribe(data => {
                this.temperature = JSON.stringify(data['temperature']);
                this.fahrenheit = JSON.stringify(data['fahrenheit']);
            });
        
    }

    check_animation() {
        this.changeGasStatus = true;

        var id = setInterval(function () {
            jQuery('#fancy').fadeOut('slow', function () {
                jQuery('#fancy').fadeIn('slow');
            });

        }, 500);
        jQuery('#btn').click(function () {
            jQuery('#fancy').removeAttr('style');
            clearInterval(id);

        });
    }
    
    public LampControll(param: any) {
        //chuyen doi qua lai
        if (parseInt(param) === 2) {
            this.value = !this.value;
            this.isChecked = this.value;
        }
        //luon bat
        else if (parseInt(param) === 1) {
            this.value = true;
            this.isChecked = true;
        }
        //luon tat
        else if (parseInt(param) === 0) {
            this.value = false;
            this.isChecked = false;
        }

        if (this.value)
            this._LightService.TurnOnLight(this.socket, 'kitchen_lamp');
        else
            this._LightService.TurnOffLight(this.socket, 'kitchen_lamp');
    }
}