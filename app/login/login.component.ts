import { Component, OnInit } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    constructor(
        private _http: Http,
        private _route: Router
    ) { }

    ngOnInit() {
        if (sessionStorage.getItem('User'))
            this._route.navigate(['/dashboard'])
    }
    createSession: any;

    Login(username: string, password: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('username', username);
        urlSearchParams.append('password', password);
        let body = urlSearchParams.toString();

        console.log(username + " " + password);

        this._http.post(`http://localhost:8080/login`, body, { headers: headers })
            .map(response => response.json())
            .subscribe(data => {
                this.createSession = JSON.stringify(data.success);
                if (this.createSession === "true") {
                   sessionStorage.setItem('User', username);
                    this._route.navigate(['/dashboard']);
                }
                else {
                    alert('Login fail!!');
                }
            });
    }
}