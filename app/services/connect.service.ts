import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from "socket.io-client";

@Injectable()
export class ConnectService {

    private url = 'http://localhost:8080';//dia chi nodejs
    private socket: any;
    private socketManager: SocketIOClient.ConnectOpts;
    interval: any;

    constructor() {

    }

    public Connect() {
        this.socket = io.connect(this.url, {
            'reconnection': true,
            'reconnectionDelay': 500,
            'reconnectionAttempts': 10
        });
    }

    SocketInstanse() {
        return this.socket;
    }
}