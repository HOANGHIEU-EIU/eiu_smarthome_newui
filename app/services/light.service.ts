import { Injectable } from '@angular/core';
import * as io from "socket.io-client";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LightService {

    constructor() { }

    public TurnOnLight(socket: any, commandOn: string) {
        socket.emit(commandOn, {
            name: "lamp",
            status: "1"
        });
    }

    public TurnOffLight(socket: any, commandOff: string) {
        socket.emit(commandOff, {
            name: "lamp",
            status: "0"
        });
    }

    public AutoLightController(socket) {
        let observable = new Observable(observer => {
            socket = io('http://localhost:8080');
            socket.on('turnON', (data) => {
                observer.next(data);
            });
            socket.on('turnOFF', (data) => {
                observer.next(data);
            });
        })
        return observable;
    }
}