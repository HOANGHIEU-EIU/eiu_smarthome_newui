import { Injectable } from '@angular/core';
import * as io from "socket.io-client";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FanService {

    constructor() { }

    public TurnOnFan(socket: any, commandOn: string) {
        socket.emit(commandOn);
    }

    public TurnOffFan(socket: any, commandOff: string) {
        socket.emit(commandOff);
    }

}