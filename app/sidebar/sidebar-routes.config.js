"use strict";
exports.ROUTES = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: 'user', title: 'User Profile', icon: 'person', class: '' },
    { path: 'table', title: 'History', icon: 'content_paste', class: '' },
    { path: 'aboutus', title: 'About Us', icon: 'home', class: '' }
];
//# sourceMappingURL=sidebar-routes.config.js.map