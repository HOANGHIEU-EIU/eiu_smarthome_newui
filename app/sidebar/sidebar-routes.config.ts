import {  RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: 'user', title: 'User Profile',  icon:'person', class: '' },
    { path: 'table', title: 'History',  icon:'content_paste', class: '' },
    { path: 'aboutus', title: 'About Us',  icon:'home', class: '' }  
   // { path: 'maps', title: 'Maps',  icon:'home', class: '' },
    
];
